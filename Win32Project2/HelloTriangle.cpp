
/*********************
The Camera System
http://learnopengl.com/#!Getting-started/Camera

************************/

#include <iostream>
#include<SOIL.h>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

#include<fstream>
#include<string>

//GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void do_movement();



// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;


const GLchar* vertexShaderSource;
const GLchar* vs;

const GLchar* fragmentShaderSource;

GLint success;
GLchar infoLog[512];



// Camera	--- the explanation of how camera works is on the game loop. View matrix
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);	// Remember that our camera is just a reverse transformation of the scene. Meaning if we go backwards, its just the entire scene is moving backwards(-z axis since opengl uses Right hand system)
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

bool keys[1024];


GLfloat yaw = -90.0f;	// Yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right (due to how Eular angles work) so we initially rotate a bit to the left.
GLfloat pitch = 0.0f;
GLfloat lastX = WIDTH / 2.0;	// calculate the offset of the mouse since the last frame. We first have to store the last mouse positions in the application, which we set to the center of the screen (screen size is 800 by 600), in our case half of 800
GLfloat lastY = HEIGHT / 2.0;	// calculate the offset of the mouse since the last frame. We first have to store the last mouse positions in the application, which we set to the center of the screen (screen size is 800 by 600), in our case half of 600


GLfloat fov = 45.0f;	// for zoom


string readfileShader(const char* filename){
	ifstream input(filename);
	if (!input.good()){
		cout << "Lol" << endl;
	}
	return std::string(istreambuf_iterator<char>(input), istreambuf_iterator<char>());
}


// The MAIN function, from here we start the application and run the game loop
int main()
{


	string temp = readfileShader("VShaders_Coordinate.glsl");
	string tempf = readfileShader("FShaders_Coodinate.glsl");

	vertexShaderSource = temp.c_str();
	fragmentShaderSource = tempf.c_str();


	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);	// tell the GFLW that we are going to use Opengl 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);	// tell the GFLW that we are going to use Opengl 3.3
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLint nrAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Hello Triangle", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);


	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);



	/******************
	NOTE:
		First we will tell GLFW that it should hide the cursor and capture it. 
		Capturing a cursor means that once the application has focus the mouse cursor stays within the window (unless the application loses focus or quits). 
		We can do this with one simple configuration call: 
		
			Sample Code:
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		After this call, wherever we move the mouse it won't be visible and it should not leave the window. This is perfect for an FPS camera system.
	
	**********************/
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	
	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);

	GLuint vertexShader;	// create a shader object
	vertexShader = glCreateShader(GL_VERTEX_SHADER);	// since we are creating a vertex shader we pass GL_VERTEX_SHADER


	/****************
	NOTE:
	glShaderSource - replace the source code of a given shader object

	1st param ------ takes the shader object
	2nd param ------ The second argument specifies how many strings we're passing as source code, which is only one.
	3rd param ------ the actual source code of the vertex shader
	4th param ------ is the array of string lenghts

	*****************/
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);	// attach the shader source code to our newly created vertex shader object
	glCompileShader(vertexShader);	// compile the shader object with the attach shader source code
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}


	/****
	same explanation as the creation of the vertex shader except we just use the fragment shader source code and GL_FRAGMENT_SHADER

	*****/
	GLuint fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	/*****************
	NOTE:
	A shader program object is the final linked version of multiple shaders combined.
	To use the recently compiled shaders we have to link them to a shader program object and then activate this shader program when rendering objects.
	The activated shader program's shaders will be used when we issue render calls

	********************/

	GLuint shaderProgram;
	shaderProgram = glCreateProgram();  // The glCreateProgram function creates a program and returns the ID reference to the newly created shader program object.

	glAttachShader(shaderProgram, vertexShader);		// the glAttachShader function attaches the compiled shader object to the shader program. In our case attach the vertex shader to our shader program
	glAttachShader(shaderProgram, fragmentShader);		// the glAttachShader function attaches the compiled shader object to the shader program. In our case attach the fragment shader to our shader program
	glLinkProgram(shaderProgram);	// links all the attach shaders into one final shader program object

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}

	// activate the shader program. Every shader and rendering call after glUseProgram will now use this program object 
	//glUseProgram(shaderProgram);		// the glUseProgram sets the given program object as the current shader program.


	/*********
	NOTE:
	Dont forget to delete the shader after we linked them to the program. Cause we no longer need them anymore.

	********/
	glDeleteShader(vertexShader);		// delete the object after linking it to the program
	glDeleteShader(fragmentShader);




	// Set up vertex data (and buffer(s)) and attribute pointers
	// More attributes: now with color
	// Set up vertex data (and buffer(s)) and attribute pointers


	GLfloat vertices[] = {
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};


	// World space positions of our cubes
	/*****************
	The reason we that its not an NDC is that the only limit we could do on our position is base on the z-axis and the far plane and near plane fustrum
	EX. if we declare the z axis as -3 and declare the x axis as 4 we wont see the cube. if we declare the z as -3 and x as 3 we could only side the 1/4 portion of the cube.
	
	******************/
	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};


	GLuint indices[] = {  // Note that we start from 0!
		0, 1, 3,   // First Triangle
		1, 2, 3    // Second Triangle
	};

	/**********************
	NOTE:

	A vertex array object (also known as VAO) can be bound just like a vertex buffer object and any subsequent vertex attribute calls from that point on will be stored inside the VAO.
	This has the advantage that when configuring vertex attribute pointers you only have to make those calls once and whenever we want to draw the object,
	we can just bind the corresponding VAO. This makes switching between different vertex data and attribute configurations as easy as binding a different VAO.
	All the state we just set is stored inside the VAO.

	A vertex array object stores the following:
	* Calls to glEnableVertexAttribArray or glDisableVertexAttribArray.
	* Vertex attribute configurations via glVertexAttribPointer.
	* Vertex buffer objects associated with vertex attributes by calls to glVertexAttribPointer.


	*************************/

	GLuint VBO, VAO;	// create a vertex buffer object(VBO) and a vertex array object(VAO)
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);	// generates one or more multiple buffer objects


	/**************
	NOTE:
		Element Buffer Objects 
			--- An EBO is a buffer, just like a vertex buffer object, that stores indices that OpenGL uses to decide what vertices to draw

	**************/

	GLuint EBO;
	glGenBuffers(1, &EBO);

	// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
	glBindVertexArray(VAO);

	/***************
	NOTE:	
			OpenGL has many types of buffer objects and the buffer type of a vertex buffer object is GL_ARRAY_BUFFER
	
	******************/
	glBindBuffer(GL_ARRAY_BUFFER, VBO);	// bind the newly created buffer object to GL_ARRAY_BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);	 // bind our Element buffer object(EBO) but this time GL_ELEMENT_ARRAY_BUFFER as the buffer type.

	/************
	NOTE:
			glBufferData is a function specifically targeted to copy user-defined data into the currently bound buffer.

			1st param ---- the type of the buffer we want to copy data into
			2nd param ---- specifies the size of the data (in bytes) we want to pass to the buffer
			3rd param ---- is the actual data we want to send
			4th param ---- The fourth parameter specifies how we want the graphics card to manage the given data. This can take 3 forms: 
				
					* GL_STATIC_DRAW: the data will most likely not change at all or very rarely.
					* GL_DYNAMIC_DRAW: the data is likely to change a lot.
					* GL_STREAM_DRAW: the data will change every time it is drawn.

	*************/

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); // glBufferData copies the previously defined vertex into the buffers memory
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW); // Note that we're now giving GL_ELEMENT_ARRAY_BUFFER as the buffer target	



	/*************
	NOTE:
		The glVertexAttribPointer specifies how Opengl should interpret the vertex buffer data whenever a drawing call is made

		1st param ----- specifies which vertex attribute we want to configure. 
						Remember that we specified the location of the position vertex attribute in the vertex shader with layout (location = 0). 
						This sets the location of the vertex attribute to 0 and since we want to pass data to this vertex attribute, we pass in 0.

		2nd param ----- specifies the size of the vertex attribute. The vertex attribute is a vec3 so it is composed of 3 values
		3rd param ----- specifies the type of the data which is GL_FLOAT (a vec* in GLSL consists of floating point values)
		4th param ----- specifies if we want the data to be normalized. 
						If we set this to GL_TRUE all the data that has a value not between 0 (or -1 for signed data) and 1 will be mapped to those values. 
						We leave this at GL_FALSE.
		5th param ----- known as the stride and tells us the space between consecutive vertex attribute sets.
						Since the next set of position data is located exactly 3 times the size of a GLfloat away we specify that value as the stride. 
						Note that since we know that the array is tightly packed (there is no space between the next vertex attribute value) we could've also 
						specified the stride as 0 to let OpenGL determine the stride (this only works when values are tightly packed).
						http://stackoverflow.com/questions/22296510/what-does-stride-means-in-opengles - best explanation of stride

		6th param ----- This is the offset of where the position data begins in the buffer. Since the position data is at the start of the data array this value is just 0. 
	
	************/

	/*********************

	NOTE: we need to update our stride and add a new attributes since we also update our shader files
	EXPLANATION:

	The first few arguments of glVertexAttribPointer are relatively straightforward. This time we are configuring the vertex attribute on attribute location 1. 
	The color values have a size of 3 floats and we do not normalize the values.

	Since we now have two vertex attributes we have to re-calculate the stride value. To get the next attribute value (e.g. the next x component of the position vector) in the data array,
	we have to move 6 floats to the right, three for the position values and three for the color values. This gives us a stride value of 6 times the size of a float in bytes (= 24 bytes).
	Also, this time we have to specify an offset. For each vertex, the position vertex attribute is first so we declare an offset of 0. 
	The color attribute starts after the position data so the offset is 3 * sizeof(GLfloat) in bytes (= 12 bytes).
	
	
	************************/
	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);	// Remember: the number 0 is the location number of the position variable declared on our Vertex Shader
	// Color attribute
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	//glEnableVertexAttribArray(1);	// Remember: the number 1 is the location number of the color variable declared on our Vertex Shader

	// TexCoord attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		5 * sizeof(GLfloat), 					// 8 because we have 3 coordinates, 3 color coordinates and 2 texture coordinates: 3 + 3 + 2 = 8
		(GLvoid*)(3 * sizeof(GLfloat)));		// we have 3 coordinates thats why its 3 it is the offset before the coordinates of texture 
	glEnableVertexAttribArray(2);	// Remember: the number 2 is the location number of the texture variable declared on our Vertex Shader



	//glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

	glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)

	// Uncommenting this call will result in wireframe polygons.
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


	// Load and create a texture 
	GLuint texture;
	GLuint texture2;

	/************
	NOTE:
		glGentexture()
		1st param ---- specifies the number of the textures object to be generated
		2nd param ---- specifies an array in which the generated texture is stored

	*************/
	glGenTextures(1, &texture);

	// Bind the texture
	glBindTexture(GL_TEXTURE_2D, texture); // All upcoming GL_TEXTURE_2D operations now have effect on this texture object


	// Set the texture wrapping parameters
	/*******************

	NOTE:
			glTexParameteri()
			1st param ------  specifies the texture target; we're working with 2D textures so the texture target is GL_TEXTURE_2D.
			2nd param ------ equires us to tell what option we want to set and for which texture axis. We want to configure the WRAP option and specify it for both the S and T axis.
			3rd param ------ equires us to pass in the texture wrapping mode we'd like and in this case OpenGL will set its texture wrapping option on the currently active texture with GL_MIRRORED_REPEAT. 

	Options:

	GL_REPEAT: The default behavior for textures. Repeats the texture image.
	GL_MIRRORED_REPEAT: Same as GL_REPEAT but mirrors the image with each repeat.
	GL_CLAMP_TO_EDGE: Clamps the coordinates between 0 and 1. The result is that higher coordinates become clamped to the edge, resulting in a stretched edge pattern.
	GL_CLAMP_TO_BORDER: Coordinates outside the range are now given a user-specified border color.

	********************/
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);


	/*******************
	Texture Filtering Options:

		GL_NEAREST ---- (also known as nearest neighbor filtering) is the default texture filtering method of OpenGL. When set to GL_NEAREST,
						OpenGL selects the pixel which center is closest to the texture coordinate. Below you can see 4 pixels where the cross represents the exact texture coordinate. 
						The upper-left texel has its center closest to the texture coordinate and is therefore chosen as the sampled color.
		
		GL_LINEAR ----- (also known as (bi)linear filtering) takes an interpolated value from the texture coordinate's neighboring texels, 
						approximating a color between the texels. The smaller the distance from the texture coordinate to a texel's center, the more that texel's color contributes 
						to the sampled color

	*****************************/

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);






	// Load image, create texture and generate mipmaps

	/*************
	ISSUES:
	You probably noticed that the texture is flipped upside-down!
	This happens because OpenGL expects the 0.0 coordinate on the y-axis to be on the bottom side of the image,
	but images usually have 0.0 at the top of the y-axis. Some image loaders like DevIL have options to reposition the y-origin during loading,
	but SOIL doesn't. SOIL does have a function called SOIL_load_OGL_texture that loads and generates a texture with a flag called SOIL_FLAG_INVERT_Y
	that solves our problem. This function does however use features that are not available in modern OpenGL so we'd have to stick with SOIL_load_image
	for now and do the texture generation ourselves. To fix this with our vertex shader we can
	We can edit the vertex shader to swap the y-coordinate automatically
	by replacing the TexCoord assignment with TexCoord = vec2(texCoord.x, 1.0f - texCoord.y);.

	**************/
	int width2, height2;
	unsigned char* image2 = SOIL_load_image("C:\\Users\\Bellchan\\Pictures\\SFML\\url.jpg", &width2, &height2, 0, SOIL_LOAD_RGB);	// we are using the SOIL library here


	/*******************
	NOTE:
		glTexImage2D() -----  Generates the texture image on the currently bound texture object

		Parameters:
			
			The first argument ----- specifies the texture target; setting this to GL_TEXTURE_2D means this operation will generate a texture on the currently bound 
									 texture object at the same target (so any textures bound to targets GL_TEXTURE_1D or G_TEXTURE_3D will not be affected).
			The second argument ---- specifies the mipmap level for which we want to create a texture for if you want to set each mipmap level manually, but we'll leave it at the base level which is 0.
			The third argument ----- tells OpenGL in what kind of format we want to store the texture. Our image has only RGB values so we'll store the texture with RGB values as well.
			The 4th, 5th argument -- sets the width and height of the resulting texture. We stored those earlier when loading the image so we'll use the corresponding variables.
			The 6th argument -------- should always be 0 (some legacy stuff).
			The 7th, 8th argument --- specify the format and datatype of the source image. We loaded the image with RGB values and stored them as chars (bytes) so we'll pass in the corresponding values.
			The 9th argument -------- is the actual image data.

	**********************/
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width2, height2, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);
	glGenerateMipmap(GL_TEXTURE_2D);



	SOIL_free_image_data(image2);
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.



	// ===================
	// Texture 2 same as loading the first texture
	// ===================
	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2);

	// Set our texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Set texture filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Load, create texture and generate mipmaps
	image2 = SOIL_load_image("C:\\Users\\Bellchan\\Pictures\\SFML\\awesomeface.png", &width2, &height2, 0, SOIL_LOAD_RGB);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width2, height2, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image2);
	glBindTexture(GL_TEXTURE_2D, 0);



	/*****************
	OpenGL stores all its depth information in a z-buffer, also known as a depth buffer. 
	GLFW automatically creates such a buffer for you (just like it has a color-buffer that stores the colors of the output image). 
	The depth is stored within each fragment (as the fragment's z value) and whenever the fragment wants to output its color, 
	OpenGL compares its depth values with the z-buffer and if the current fragment is behind the other fragment it is discarded, otherwise overwritten.
	This process is called depth testing and is done automatically by OpenGL. 
	
	******************/
	glEnable(GL_DEPTH_TEST);


	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		do_movement();

		// Render
		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

		/****************
		nce we're using a depth buffer we also want to clear the depth buffer before each render iteration (otherwise the depth information of the previous frame stays in the buffer). Just like clearing the color buffer, we can clear the depth buffer by specifying the DEPTH_BUFFER_BIT bit in the glClear function
		
		*****************/
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		// Bind Texture
		//glBindTexture(GL_TEXTURE_2D, texture);

		 glUseProgram(shaderProgram);

	




		 // Bind Textures using texture units
		 /******************
		 NOTE:
		 The main purpose of texture units is to allow us to use more than 1 texture in our shaders. 
		 By assigning texture units to the samplers, we can bind to multiple textures at once as long as we activate the corresponding texture unit first.
		 Just like glBindTexture we can activate texture units using glActiveTexture passing in the texture unit we'd like to use:
		 
		 ********************/
		 glActiveTexture(GL_TEXTURE0);	// Activate the texture unit first before binding texture
		 glBindTexture(GL_TEXTURE_2D, texture);
		 glUniform1i(glGetUniformLocation(shaderProgram, "ourTexture1"), 0);	// Remeber the "ourTexture1" variable is on the fragment shader

		 glActiveTexture(GL_TEXTURE1);	// Activate the texture unit first before binding texture
		 glBindTexture(GL_TEXTURE_2D, texture2);
		 glUniform1i(glGetUniformLocation(shaderProgram, "ourTexture2"), 1);



		
		 
		 /**************** How camera works and how it is made using mathematics(Linear Algebra)

		 EXPLANATION:

		 Camera/View space
		 -----	When we're talking about camera/view space we're talking about all the vertex coordinates as seen from the camera's perpective as the origin
				 of the scene: the view matrix transforms all the world coordinates into view coordinates that are relative to the camera's position and direction.
				 To define a camera we need its position in world space, the direction it's looking at, a vector pointing to the right and a vector pointing upwards from the camera.
				 A careful reader might notice that we're actually going to create a coordinate system with 3 perpendicular unit axes with the camera's position as the origin.

			 The CAMERA POSITION:
					-> basically a vector in world space that points to the camera's position.

						 Sample code:
						 glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);


			 The CAMERA DIRECTION:
					 ->	The next vector required is the camera's direction e.g. at what direction it is pointing at. For now we let the camera point to the origin of our scene: (0,0,0).
						 Remember that if we subtract two vectors from each other we get a vector that's the difference of these two vectors?
						 Subtracting the camera position vector from the scene's origin vector thus results in the direction vector.
						 Since we know that the camera points towards the negative z direction we want the direction vector to point towards the camera's positive z-axis.
						 If we switch the subtraction order around we now get a vector pointing towards the camera's positive z-axis:

							 Sample Code:
							 glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
							 glm::vec3 cameraDirection = glm::normalize(cameraPos - cameraTarget);

			 The RIGHT AXIS
				 ->	The next vector that we need is a right vector that represents the positive x-axis of the camera space.
					 To get the right vector we use a little trick by first specifying an up vector that points upwards (in world space).
					 Then we do a cross product on the up vector and the direction vector from step 2. Since the result of a cross product is a vector perpendicular to both vectors,
					 we will get a vector that points in the positive x-axis's direction (if we would switch the vectors we'd get a vector that points in the negative x-axis):

						 Sample Code:
						 glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
						 glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));

			 The UP AXIS
				 ->	Now that we have both the x-axis vector and the z-axis vector, retrieving the vector that points in the camera's positive y-axis is
					relatively easy: we take the cross product of the right and direction vector:

					 Sample Code:
					 glm::vec3 cameraUp = glm::cross(cameraDirection, cameraRight);


		 With the help of the cross product and a few tricks we were able to create all the vectors that form the view/camera space.
		 For the more mathematically inclined readers, this process is known as the Gram-Schmidt process in linear algebra. Using these camera vectors we can
		 now create a LookAt matrix that proves very useful for creating a camera.

		 ******************************************/
		 glm::mat4 view;


		 /****************

		 NOTE:

			glm::lookAt()	----- creates a view matrix that will act as our camera

			1st param ----- Position - specifies the position of the camera in the world coordinates
			2nd param ----- Target	 - specifies the target/direction of the camera should look at. In our case its the origin
			3rd param ----- Up vector - specifies the positive y-direction use to create the right vector using the cross product


			Code Explanation:

				We use a little bit of trigonometry to create an x and z coordinate each frame that represents a point on a circle and we'll use these for our camera position.
				By re-calculating the x and y coordinate we're traversing all the points in a circle and thus the camera rotates around the scene. 
				We enlarge this circle by a pre-defined radius and create a new view matrix each render iteration using GLFW's glfwGetTime function:

		 ****************/
	
		 view = glm::lookAt(cameraPos,	// first we specify the camera position
			 cameraPos + cameraFront,	// The direction is the current position + the direction vector we just defined. This ensures that however we move, the camera keeps looking at the target direction.
			 cameraUp);

		 // Create transformations
		 glm::mat4 model;
		 glm::mat4 projection;
	


		 /*************
			NOTE:
				When scrolling, the yoffset value represents the amount we scrolled vertically. 
				When the scroll_callback function is called we change the content of the globally declared fov variable. 
				Since 45.0f is the default fov value we want to constrain the zoom level between 1.0f and 45.0f.

		 *************/
		 projection = glm::perspective(fov, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);	// replace the static number on the first param with fov variable to be able to make zoom illusion



		 // Get their uniform location
		 GLint modelLoc = glGetUniformLocation(shaderProgram, "model");
		 GLint viewLoc = glGetUniformLocation(shaderProgram, "view");
		 GLint projLoc = glGetUniformLocation(shaderProgram, "projection");


		 // Pass them to the shaders
		 glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		 glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		 // Note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
		 glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));




        glBindVertexArray(VAO);
		for (GLuint i = 0; i < 10; i++)
		{
			// Calculate the model matrix for each object and pass it to shader before drawing
			glm::mat4 model;
			model = glm::translate(model, cubePositions[i]);
			GLfloat angle = 20.0f * i;
			model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
        glBindVertexArray(0);

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();



	return 0;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	std::cout << key << std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);


	/*********************
	NOTE:

		you can't move in two directions at the same time (diagonal movement) and when you hold down one of the keys, it first bumps a little and after a short break starts moving. 
		This happens because most event-input systems can handle only one keypress at a time and their functions are only called whenever we activate a key. 
		While this works for most GUI systems, it is not very practical for smooth camera movement. We can solve the issue by showing you a little trick.

		The trick is to only keep track of what keys are pressed/released in the callback function. 
		In the game loop we then read these values to check what keys are active and react accordingly. So we're basically storing state information about what keys are pressed/released 
		and react upon that state in the game loop. First, let's create a boolean array representing the keys pressed/released:
	
	**********************/



	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			keys[key] = true;
		else if (action == GLFW_RELEASE)
			keys[key] = false;
	}
}




void do_movement()
{
	// Camera controls . Keep in mind that we are using Right hand system. That means that +z is poiting towards you.
	GLfloat cameraSpeed = 0.01f;
	if (keys[GLFW_KEY_W]) {
		cameraPos += cameraSpeed * cameraFront;	// keep in mind that the cameraFront z value is -1;
		std::cout << "Camera postiion: " << cameraPos.z << " Camera front is : " << cameraFront.z << std::endl;
	}
		
	if (keys[GLFW_KEY_S])
	{
		cameraPos -= cameraSpeed * cameraFront;	// keep in mind that the cameraFront z value is -1;
		std::cout << "Camera postiion: " << cameraPos.z << " Camera front is : " << cameraFront.z << std::endl;
	}
		
	/***************************
	NOTE:

		 We normalize the resulting right vector. If we wouldn't normalize this vector, the resulting cross product might return differently sized vectors based on the cameraFront variable. 
		 If we would not normalize the vector we would either move slow or fast based on the camera's orientation instead of at a consistent movement speed.
	
	**********************************/

	if (keys[GLFW_KEY_A])
		cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (keys[GLFW_KEY_D])
		cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;

	
}


/*********************
NOTE:
	To calculate the pitch and yaw values we need to tell GLFW to listen to mouse-movement events. We do this (simliar to keyboard input) by creating a callback function with the following prototype:

**************************/

bool firstMouse = true;
void mouse_callback(GLFWwindow* window, double xpos /** X cursor position **/, double ypos /** Y cursor position **/)
{




	/*================================

		When handling mouse input for an FPS style camera there are several steps we have to take before eventually retrieving the direction vector:

			* Calculate the mouse's offset since the last frame.
			* Add the offset values to the camera's yaw and pitch values.
			* Add some constraints to the maximum/minimum yaw/pitch values
			* Calculate the direction vector

	=================================*/






	/******************************
	NOTE:
			If you would now run the code you will notice that the camera makes a large sudden jump whenever the window first receives focus of your mouse cursor. 
			The cause for the sudden jump is that as soon as your cursor enters the window the mouse callback function is called with an xpos and ypos position equal to the location your mouse entered the screen.
			This is usually a position that is quite a distance away from the center of the screen resulting in large offsets and thus a large movement jump. 
			We can circumvent this issue by simply defining a global bool variable to check if this is the first time we receive mouse input and if so, 
			we first update the initial mouse positions to the new xpos and ypos values; 
			the resulting mouse movements will then use the entered mouse's position coordinates to calculate its offsets.
	
	*******************************/
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos; // Reversed since y-coordinates go from bottom to left
	lastX = xpos;
	lastY = ypos;


	/***************
	NOTE:
		We multiply the offset values by a sensitivity value. If we omit this multiplication the mouse movement would be way too strong; fiddle around with the sensitivity value to your liking.

	******************/
	GLfloat sensitivity = 0.05;	// Change this value to your liking
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (pitch > 89.0f)	// at 90 degrees the view tends to reverse, so we stick to 89 as our limit
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;



	/***********************
	NOTE:

	Euler angles

	Euler angles are 3 values that can represent any rotation in 3D, defined by Leonhard Euler somewhere in the 1700s. There are 3 Euler angles: pitch, yaw and roll. 

		* The pitch is the angle that depicts how much we're looking up or down.  Imagine rotating around the x-axis (NOTE: rotation is different from 2D)
		* The second image shows the yaw value which represents the magnitude we're looking to the left or to the right. Imaging rotating around y-axis
		* The roll represents how much we roll as mostly used in space-flight cameras. Imagine rotating around z-axis

		Each of the Euler angles are represented by a single value and with the combination of all 3 of them we can calculate any rotation vector in 3D.

	
	************************/
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	cameraFront = glm::normalize(front);
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	/*************
		NOTE:
			When scrolling, the yoffset value represents the amount we scrolled vertically.
			When the scroll_callback function is called we change the content of the globally declared fov variable.
			Since 45.0f is the default fov value we want to constrain the zoom level between 1.0f and 45.0f.

	*************/

	fov -= yoffset;
	std::cout << "offset " << fov << std::endl;

	/*if (fov >= 1.0f && fov <= 45.0f)
		fov -= yoffset;
	if (fov <= 1.0f)
		fov = 1.0f;
	if (fov >= 45.0f)
		fov = 45.0f;*/
}